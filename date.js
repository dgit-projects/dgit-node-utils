const paddedTwoDigits = num => num.toString().padStart(2, 0)

class DateUtils {
  static toLocalDate(d) {
    const localDate = new Date(d.getTime() - d.getTimezoneOffset() * 60000)
    return localDate
  }

  static getFormattedDate(date) {
    if (!date) {
      return ''
    }
    return DateUtils.format(date, "%F")
  }

  static getFormattedTime(date) {
    if (!date) {
      return ''
    }
    return DateUtils.format(date, "%T")
  }

  static toTimestamp(date) {
    if (!date) {
      return ''
    }
    return DateUtils.format(date, "%Y%m%d%H%M%S")
  }

  static format(date, formatString) {
    if (!(date instanceof Date)) {
      throw `${date} is not a date`
    }
    const formattedDate = formatString
      .replace("%Y", date.getFullYear())
      .replace("%y", paddedTwoDigits(date.getYear() % 100))
      .replace("%m", paddedTwoDigits(date.getMonth()+1))
      .replace("%d", paddedTwoDigits(date.getDate()))
      .replace("%H", paddedTwoDigits(date.getHours()))
      .replace("%M", paddedTwoDigits(date.getMinutes()))
      .replace("%S", paddedTwoDigits(date.getSeconds()))
      .replace("%F", `${date.getFullYear()}-${paddedTwoDigits(date.getMonth()+1)}-${paddedTwoDigits(date.getDate())}`)
      .replace("%T", `${paddedTwoDigits(date.getHours())}-${paddedTwoDigits(date.getMinutes())}-${paddedTwoDigits(date.getSeconds())}`)
    return formattedDate
  }

  static map(startDate, endDate, f) {
    const retval = []
    if (!startDate || !endDate) {
      return retval
    }
    for (let d = new Date(startDate); d <= endDate; d.setDate(d.getDate() + 1)) {
      // console.log("pushing date", d)
      retval.push(f(d))
    }
    return retval
  }

}

module.exports = DateUtils
