const parser = require('@babel/parser')
const traverse = require('@babel/traverse').default
const template = require('@babel/template').default
const t = require('@babel/types')

const generate = require('@babel/generator').default
const prettier = require("prettier")

const ArrayUtils = require('./array')

class AstUtils {
  static isOutermostRequireAssignment(path) {
    return path.isVariableDeclaration() && path.parentPath.isProgram() && path.node.declarations[0].init.type === 'CallExpression' && path.node.declarations[0].init.callee.name === 'require'
  }

  static insertLastRequire(ast, identifierName, requirePath, deconstructed = false) {
    let inserted = false
    let code

    if (deconstructed) {
      // TODO: support append to an already existing deconstructed require
      code = `const { ${identifierName} } = require('${requirePath}')`
    }
    else {
      code = `const ${identifierName} = require('${requirePath}')`
    }
    const codeAst = parser.parse(code, { sourceType: 'module' })

    traverse(ast, {
      Program(path) {
        if(!inserted) {
          // File is empty
          const firstStatementNodePath = path.get("body")[0]
          if (!firstStatementNodePath) {
            path.replaceWith(codeAst.program)
            inserted = true
            return
          }
          // File is not empty but there are no requires
          const lastRequireNodePath = path.get("body").filter(p => AstUtils.isOutermostRequireAssignment(p)).pop()
          if (!lastRequireNodePath) {
            firstStatementNodePath.insertBefore(codeAst.program.body[0])
            inserted = true
            return
          }

          // There are requires
          // TODO: Do not add if the same require already exists
          lastRequireNodePath.insertAfter(codeAst.program.body[0])
          inserted = true
          return
        }
      }
    })
  }

  static insertCodeAtProgramLevel(ast, code) {
    const codeAst = parser.parse(code, { sourceType: 'module' }).program
    let inserted = false
    traverse(ast, {
      Program(path) {
        if (!inserted) {
          // File is empty
          const bodyLastNodePath = ArrayUtils.last(path.get("body"))
          if (!bodyLastNodePath === 0) {
            path.replaceWith(codeAst)
            inserted = true
            return
          }

          // File has at least one statement
          bodyLastNodePath.insertAfter(codeAst.body[0])
          inserted = true
          return
        }
      }
    })
  }

  static insertMethodIfNotPresent(ast, klassName, methodName) {
    const methodCode = `
class Temp {
  ${methodName}() {}
}`
    const methodCodeAst = template(methodCode)().body
    let inserted = false
    traverse(ast, {
      ClassDeclaration(path) {
        if (path.node.id.name === klassName) {
          if (!inserted) {
            // TODO: Handle static, async etc.
            const methodPresent = path.get("body").get("body").some(p => p.node.key.name === methodName && !p.node.static)
            if (!methodPresent) {
              // Class has no class methods
              const lastMethodNodePath = path.get("body").get("body").filter(p => p.isClassMethod()).pop()
              if (!lastMethodNodePath) {
                path.get("body").replaceWith(methodCodeAst)
                inserted = true
                return
              }

              // Class has at least one class method
              lastMethodNodePath.insertAfter(methodCodeAst.body[0])
              inserted = true
              return
            }
          }
        }
      }
    })
  }

  static insertCodeInClassMethod(ast, classMethodName, code) {
    const codeAst = template(code)()
    let inserted = false
    traverse(ast, {
      ClassMethod(path) {
        if (!inserted && path.node.key.name === classMethodName) {

          // Method is empty
          const firstStatementNodePath = path.get("body").get("body")[0]
          if (!firstStatementNodePath) {
            const functionCode = `
class Temp {
  ${classMethodName}() {
    ${code}
  }
}`
            const functionCodeAst = template(functionCode)().body.body[0]
            // console.log(functionCodeAst)
            path.replaceWith(functionCodeAst)
            inserted = true
            return
          }

          // Method has at least one statement
          firstStatementNodePath.insertBefore(codeAst)
          inserted = true
          return
        }
      }
    })
  }

  static generateCode(ast, prettierConfig = {}) {
    const { code } = generate(ast, { retainLines: true/* Options */ }, '')
    const prettierCode = prettier.format(code, { parser: 'babel', ...prettierConfig })
    return prettierCode
  }

  static parse(code) {
    return parser.parse(code, { sourceType: 'module' })
  }
}

module.exports = AstUtils
