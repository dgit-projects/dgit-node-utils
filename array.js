class ArrayUtils {
  static last(array) {
    return array[array.length - 1]
  }

  static sample(array) {
    return array[Math.floor(Math.random()*array.length)];
  }

  static uniqueAppend(array, ...elementOrElements) {
    return ArrayUtils.unique([...array, ...elementOrElements])
  }

  static union(array1, array2) {
    return ArrayUtils.unique([...array1, ...array2])
  }

  static difference(array1, array2) {
    return array1.filter(x => !array2.includes(x))
  }

  static intersection(array1, array2) {
    return array1.filter(x => array2.includes(x))
  }

  static unique(array) {
    return [...new Set([...array])]
  }

  static wrap(elementOrArray) {
    if (Array.isArray(elementOrArray)) {
      return elementOrArray
    }
    return [elementOrArray]
  }

  static max(array, comparer = (a, b) => (a > b) ? a : b) {
    if (!Array.isArray(array)) {
      return undefined
    }
    if (array.length === 0) {
      return undefined
    }
    return array.reduce(comparer)
  }

  static min(array, comparer = (a, b) => (a < b) ? a : b) {
    if (!Array.isArray(array)) {
      return undefined
    }
    if (array.length === 0) {
      return undefined
    }
    return array.reduce(comparer)
  }

  static sum(array) {
    return array.reduce((accumulator, currentValue) => accumulator + currentValue, 0)
  }

  static async asyncForEach(array, f) {
    for (let index = 0; index < array.length; ++index) {
      await f(array[index], index, array)
    }
  }

  static isEmpty(array) {
    return !(array && array.length && array.length > 0)
  }
}

module.exports = ArrayUtils
