const DateUtils = require('./date')

class DateLogger {
  static log(...args) {
    console.log(DateUtils.toLocalDate(new Date), ...args)
  }
}

module.exports = DateLogger
