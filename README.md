# dgit-node-utils

The aim is not to replace lodash but to extend it.<br/>
Some functionalities are already provided by lodash and in later releases, we will use this package as a wrapper around those lodash functionalities.
