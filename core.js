const DateLogger = require('./datelogger')
class CoreUtils {
  static sleep(ms) {
    DateLogger.log(`sleeping for ${ms} ms`)
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  static methods(object) {
    return Object.getOwnPropertyNames(Object.getPrototypeOf(object)).filter(n => !['constructor', 'definition'].includes(n))
  }
}

module.exports = CoreUtils
